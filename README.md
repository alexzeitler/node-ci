# node-seed
Create Node.js projects fast

Run [node-seed](https://github.com/PDMLab/dotfiles-zsh/blob/master/.aliases#L41) `zsh` alias from https://github.com/PDMLab/dotfiles-zsh to get started even faster.
